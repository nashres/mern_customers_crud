import React, {Component} from "react";
import styled from 'styled-components';
import {Link} from 'react-router-dom';


const Collapse = styled.div.attrs({
    className : 'collapse navbar-collapse'
})``

const Item = styled.div.attrs({
    className: 'collapse navbar-collapse'
})` 
 &:hover {
    background-color: #340650;
    color: black;
  }
`

const List = styled.div.attrs({
    className: 'navbar-nav mr-auto'
})``




class Links extends Component {
    render() {
        return(
            <React.Fragment>
                <Link to="#" className="navbar-brand">MERN CRUD</Link>
                <Collapse>
                    <List>
                        <Item>
                            <Link to="/customer/list" className="nav-link"> List Customers</Link>
                        </Item>
                        <Item>
                            <Link to="/customer/create" className="nav-link"> Create Customer</Link>
                        </Item>
                    </List>
                </Collapse>
            </React.Fragment>
        );
    }
}

export default Links;