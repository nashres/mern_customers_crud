import CustomerInsert from './CustomerInsert';
import CustomersList from './CustomersList';
import CustomerUpdate from './CustomerUpdate';

export {CustomerInsert, CustomersList, CustomerUpdate};