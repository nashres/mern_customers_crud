const Customer = require('../db/model/customer-model');

createCustomer = (req, res) => {
    
    const body = req.body;
    if(!body) {
        return res.status(400).json({success:false, error: "You must provide an customer"});
    }

    const customer = new Customer(body);
    if(!customer) {
        return res.status(400).json({success:false, error: "Customer creation failed."});
    }

    customer.save().then(()=> {
        return res.status(200).json({success: true, id: customer._id, message: "Customer created."}); //customer._id comes back automatically bc mongodb create it
    })
    .catch(error => {
        return res.status(404).json({error, message: "Customer not created"});
    })
    
};

UpdateCustomer = async (req, res) => {
    //get json object from request body
    const body = req.body;
    if(!body) {
        return res.status(400).json({success:false, error: "You must provide an customer to update"});
    }
    const customer = new Customer(body);
    //find the document in the database
    Customer.findOne({_id: req.params.id}, (err, customer) => {
        if(err) {
            return res.status(400).json({err, message: "Customer item not found"});
        }
        //update the document with the json data that was passed in the re.body
        customer.customer = body.customer;
        customer.artist = body.artist;
        customer.year = body.year;
        customer.artwork = body.artwork; 

        //save the updated object back to the database
        customer.save().then(() => {
            return res.status(200).json({success: true, id: customer._id, message: "Customer updated."});
        })
        .catch(error => {
            return res.status(404).json({error, message: "Customer not updated"});

        });
    });
   
    
};



deleteCustomer = async (req, res) => {
    Customer.findOneAndDelete({_id: req.params.id}, (err, customer) => {
        if(err) {
            return res.status(400).json({success: false, error: err});
        }
        if(!customer) {
            return res.status(404).json({success:false, error: "Customer not found"});
        }
        return res.status(200).json({success:true, data: customer});
        
    })
};


getCustomerById = async (req, res) => {
    Customer.findById(req.params.id, function(err, customer) {
        if(err) {
            return res.status(400).json({success: false, error: err});
        }
        if(!customer) {
            return res.status(404).json({success:false, error: "Customer not found"});
        }
        console.log(customer)
        return res.status(200).json({success:true, data: customer});
    });
};


getAllCustomers = async (req, res) =>{
    Customer.find({}, (err, customers) => {
        if(err) {
            return res.status(400).json({success: false, error: err});
        }
        if(!customers.length) {
            return res.status(404).json({success: false, error: "No customers found."});
        }
        return res.status(200).json({success: true, data: customers});
    });
};



module.exports = {
    createCustomer,
    UpdateCustomer,
    deleteCustomer,
    getCustomerById,
    getAllCustomers
}