import React, {Component} from 'react';
import ReactTable from 'react-table';
import apis from '../api';
import styled from 'styled-components';


const Title = styled.h1.attrs({
    className: 'h1',
})``

const Wrapper = styled.div.attrs({
    claxssName: 'form-group',
})`
    margin: 0 30px;
`

const Label = styled.label`
    margin: 5px;
`

const InputText = styled.input.attrs({
    className: 'form-control',
})`
    margin: 5px;
`

const Button = styled.button.attrs({
    className: `btn btn-primary`,
})`
    margin: 15px 15px 15px 5px;
`

const CancelButton = styled.a.attrs({
    className: `btn btn-danger`,
})`
    margin: 15px 15px 15px 5px;
`

class CustomerUpdate extends Component{
    constructor(props) {
        super(props)
        this.state ={
            id: this.props.match.params.id,
            name:'',
            email:'',
            phone:'',
            profession:'',
        }
    }

    handleChangeInputName = async event => {
        const name = event.target.value;
        this.setState({name});
    }
    handleChangeInputEmail = async event => {
        const email = event.target.value;
        this.setState({email});
    }
    handleChangeInputPhone = async event => {
        const phone = event.target.validity.valid
        ? event.target.value : this.state.phone;
        this.setState({phone})

    }
    handleChangeInputProfession = async event => {
        const profession = event.target.value;
        this.setState({profession})

    }

    handleUpdateCustomer = async event => {
        const {id, name, email, phone, profession} = this.state;
        const payload = {name, email, phone, profession};

        await apis.updateCustomerById(id, payload).then(res => {
            window.alert('Customer updated successfully.');
            this.setState({
                name:'',
                email:'',
                phone:'',
                profession:'',
            });
            this.props.history.push('/customer/list');
        })
    }

    componentDidMount = async () => {
        const {id} = this.state;
        const customer = await apis.getCustomerById(id);
        console.log("Customer")
        console.log(customer)
        this.setState({
            name: customer.data.data.name,
            email: customer.data.data.email,
            phone: customer.data.data.phone,
            profession: customer.data.data.profession,
        })
    }

    render() {
        const {customer, artist, year, artwork} = this.state;
        return (
            <Wrapper>
                <Title>Update Customer</Title>

                <Label>Customer Name:</Label>
                <InputText
                    type="text"
                    value = {this.state.name}
                    onChange={this.handleChangeInputName} />
                
                <Label>Artist Email:</Label>
                <InputText
                    type="text"
                    value = {this.state.email}
                    onChange={this.handleChangeInputEmail} />
                
                <Label>Customer Phone</Label>
                <InputText
                    type="text"
                    value = {this.state.phone}
                    onChange={this.handleChangeInputPhone} />

                <Label>Customer Profession</Label>
                <InputText
                    type="text"
                    value = {this.state.profession}
                    onChange={this.handleChangeInputProfession} />


                <Button onClick={this.handleUpdateCustomer}>Update Customer</Button>
                <CancelButton href={'/customer/list'}>Cancel</CancelButton>
            </Wrapper>
        )
    }
}


export default CustomerUpdate;

