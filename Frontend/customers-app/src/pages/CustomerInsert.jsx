import React, {Component} from 'react';
import ReactTable from 'react-table';
import apis from '../api';
import styled from 'styled-components';


const Title = styled.h1.attrs({
    className: 'h1',
})``

const Wrapper = styled.div.attrs({
    className: 'form-group',
})`
    margin: 0 5em;
`

const Label = styled.label`
    margin: 5px;
`

const InputText = styled.input.attrs({
    className: 'form-control',
})`
    margin: 5px;
`

const Button = styled.button.attrs({
    className: `btn btn-primary`,
})`
    margin: 15px 15px 15px 5px;
`

const CancelButton = styled.a.attrs({
    className: `btn btn-danger`,
})`
    margin: 15px 15px 15px 5px;
`


class CustomerInsert extends Component {
    constructor(props) {
        super(props)
        this.state ={
            name:'',
            email:'',
            phone:'',
            profession:'',
        }
    }

    handleChangeInputName = async event => {
        const name = event.target.value;
        this.setState({name});
    }
    handleChangeInputEmail = async event => {
        const email = event.target.value;
        this.setState({email});
    }
    handleChangeInputPhone = async event => {
        const phone = event.target.validity.valid
        ? event.target.value : this.state.phone;
        this.setState({phone})

    }
    handleChangeInputProfession = async event => {
        const profession = event.target.value;
        this.setState({profession})

    }

    handleInsertCustomer = async event => {
        const {name, email, phone, profession} = this.state;
        const payload = {name, email, phone, profession};

        await apis.insertCustomer(payload).then(res => {
            window.alert('Customer Added successfully.');
            this.setState({
                name:'',
                email:'',
                phone:'',
                profession:''
            });
            //this.props.history.push('/music/list');
        })
    }

    render() {
        return (
            <Wrapper>
                <Title>Create Customer</Title>

                <Label>Customer Name:</Label>
                <InputText
                    type="text"
                    value = {this.state.name}
                    onChange={this.handleChangeInputName} />
                
                <Label>Artist Email:</Label>
                <InputText
                    type="text"
                    value = {this.state.email}
                    onChange={this.handleChangeInputEmail} />
                
                <Label>Customer Phone</Label>
                <InputText
                    type="text"
                    value = {this.state.phone}
                    onChange={this.handleChangeInputPhone} />

                <Label>Customer Profession</Label>
                <InputText
                    type="text"
                    value = {this.state.profession}
                    onChange={this.handleChangeInputProfession} />

                <Button onClick={this.handleInsertCustomer}>Add Customer</Button>
                <CancelButton href={'/customers/list'}>Cancel</CancelButton>
            </Wrapper>
        )
    }
}


export default CustomerInsert;