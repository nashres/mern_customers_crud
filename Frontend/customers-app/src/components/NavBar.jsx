import React, {Component} from "react";
import styled from 'styled-components';

import Logo from './Logo';
import Links from './Links';

const Container = styled.div.attrs({
    className : 'container'
})`height: 150px;
    ` 

const Nav = styled.div.attrs({
    className : 'navbar navbar-expand-lg navbar-dark '
})`margin:-bottom: 20px;
background-color: #7d3caf!important;
padding:1em;
`

class NavBar extends Component {
    render() {
        return(
            <Container>
                <Nav>
                    <Logo />
                    <Links />
                </Nav>
            </Container>
        );
    }
}

export default NavBar;