import React, {Component} from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import {CustomersList, CustomerInsert, CustomerUpdate} from '../pages'; //note this is import from index.js file inside the directory 'pages'
import "bootstrap/dist/css/bootstrap.min.css";
import NavBar from '../components/NavBar';

class App extends Component{
    render() {
        return(
            <Router>
                <NavBar />
                <Switch>
                    <Route path="/customer/list" exact component={CustomersList} />
                    <Route path="/customer/create" exact component={CustomerInsert} />
                    <Route path="/customer/update/:id" exact component={CustomerUpdate} />
                </Switch>
            </Router>
        )
    }
        
    
}

export default App;