
const express = require('express');

const CustomerController = require("../controller/controller.js");
const router = express.Router();


//SHOW ROUTES
//GET ALL
router.get('/customer',CustomerController.getAllCustomers);
//GET ONE
router.get('/customer/:id',CustomerController.getCustomerById );
//CREATE ROUTE
router.post('/customer',CustomerController.createCustomer );

//UPDATE ROUTE
router.put('/customer/:id',CustomerController.UpdateCustomer );
//DELETE ROUTE
router.delete('/customer/:id',CustomerController.deleteCustomer );


module.exports = router;