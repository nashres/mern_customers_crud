import React, {Component} from 'react';
import ReactTable from 'react-table';
import apis from '../api';
import styled from 'styled-components';
import 'react-table/react-table.css'

const Wrapper = styled.div`
    padding: 0 40px 40px 40px;
`

const Update = styled.div`
    color: #d8d8eb;
    cursor: pointer;
    background-color:#7d3baf;
`

const Delete = styled.div`
    color: #FFFFFF;
    background-color:red;
    cursor: pointer;
`

//just want to try functional components
class UpdateCustomer extends Component {
    updateUser = event => {
        event.preventDefault();
        window.location.href = `/customer/update/${this.props.id}`
    }
    render() {
        return <Update onClick={this.updateUser}>Update</Update>;
    }
        
    
}


class DeleteCustomer extends Component {
    deleteUser = event => {
        event.preventDefault();
        if(window.confirm(
            'Do you want to delete the selected customer permanently?'
        )) {
            apis.deleteCustomerById(this.props.id);
            window.location.reload();
        }
    }
    render() {
        return <Delete onClick={this.deleteUser}>Delete</Delete>;
    }
}

class CustomersList extends Component {
    constructor(props) {
        super(props)
        this.state = {
            customers: [],
            isLoading: false
        }
    }

    componentDidMount = async() => {
        this.setState({isLoading: true});

        await apis.getAllCustomers().then(customers => {
            this.setState({
                customers: customers.data.data,
                isLoading: false
            })
        }

        )
    }

    render() {
        const {customers, isLoading} = this.state;
        const columns = [
            {
                Header: 'Customer Name',
                accessor: 'name',
                Cell: row=> <div style = {{textAlign:"center"}}>{row.value}</div>
            },
            {
                Header: 'Customer Email',
                accessor: 'email',
                Cell: row=> <div style = {{textAlign:"center"}}>{row.value}</div>
            },
            {
                Header: 'Customer Phone',
                accessor: 'phone',
                Cell: row=> <div style = {{textAlign:"center"}}>{row.value}</div>
            },
            {
                Header: 'Customer Profession',
                accessor: 'profession',
                Cell: row=> <div style = {{textAlign:"center"}}>{row.value}</div>
            },
            {
                Header:'',
                accessor:'',
                Cell: (props) => {
                    return(
                        <span>
                            <DeleteCustomer id={props.original._id} />
                        </span>
                    )
                }
            },
            {
                Header:'',
                accessor:'',
                Cell: (props) => {
                    return(
                        <span>
                            <UpdateCustomer id={props.original._id} />
                        </span>
                    )
                }
            }
        ]

        let showTable = true;
        if(!customers.length) {
            showTable = false;
        }
        return(
            <Wrapper>
                {console.log(customers)}
                {showTable && (
                    <ReactTable 
                        data = {customers}
                        columns = {columns}
                        loading = {isLoading}
                        defaultPageSize = {10}
                        showPageSizeOptions={true}
                        minRows={0}


                    />
                )}
                {!showTable?<h2>The list of customers is empty</h2>:""}

            </Wrapper>
        );
    }
}

export default CustomersList;