//const bodyParser = require('body-parser');
const mongoose = require('mongoose');  


mongoose.connect("mongodb://localhost:27017/music_app", {useNewUrlParser:true})
.catch(e => {
    console.error('Connection error', e.message);
});

const db = mongoose.connection;

module.exports = db;