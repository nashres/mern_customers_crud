var express = require('express'),   //var or const doesnt matter, it wont change anyway
    backend = express(),  //this comes the ar express not the library expresss
    db = require("./db/connect.js"),
    router = require('./routes/route.js'),
    cors = require('cors'); //allowing to share between this and this one over here

backend.use(express.json()); //make express use bodyparser, that's to parse data from db to here as json object
backend.use(cors({
    origin: 'http://localhost:3000'
}));


//ROUTES

//INDEX
backend.get('/', (req,res) => {
    res.redirect('/customer');
});


backend.use('/api', router);





backend.listen(3010, () => {
    console.log("server started successfully ...");
});

