const mongoose = require('mongoose');  


var customerSchema = new mongoose.Schema({
    name: {type: String, required: false},
    email: {type: String, required: false},
    phone: {type: Number, required: false},
    profession: {type: String, required: false},
});

module.exports = mongoose.model("Customer", customerSchema);
